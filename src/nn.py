import os
import struct
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable

DEFAULT_IMG_FILE = 'train-images.idx3-ubyte'
DEFAULT_LBL_FILE = 'train-labels.idx1-ubyte'
DEFAULT_MODEL_FILE = 'trained_network'

LBL_MAGIC_NUM = 2049
IMG_MAGIC_NUM = 2051

IMG_ROWS = 28
IMG_COLS = 28


class NeuralNetwork(torch.nn.Module):

    def __init__(self, inodes, hnodes, onodes, learning_rate):
        super().__init__()
        self.inodes = inodes
        self.hnodes = hnodes
        self.onodes = onodes
        self.lr = learning_rate
        self.linear_ih = nn.Linear(inodes, hnodes, bias=False)
        self.linear_ho = nn.Linear(hnodes, onodes, bias=False)
        self.activation = nn.Sigmoid()
        self.error_function = torch.nn.MSELoss(size_average=False)
        self.optimiser = torch.optim.SGD(self.parameters(), learning_rate)
        pass

    def forward(self, inputs_list):
        inputs = Variable(torch.FloatTensor(inputs_list).view(1, self.inodes))
        hidden_inputs = self.linear_ih(inputs)
        hidden_outputs = self.activation(hidden_inputs)
        final_inputs = self.linear_ho(hidden_outputs)
        final_outputs = self.activation(final_inputs)
        return final_outputs

    def train(self, inputs_list, targets_list):
        output = self.forward(inputs_list)
        target_variable = Variable(
                torch.FloatTensor(targets_list).view(1, self.onodes),
                requires_grad=False)
        loss = self.error_function(output, target_variable)
        self.optimiser.zero_grad()
        loss.backward()
        self.optimiser.step()
        pass

def read_dataset(img_file=DEFAULT_IMG_FILE, lbl_file=DEFAULT_LBL_FILE, path=".", transform=None):
    img_file = os.path.join(path, img_file)
    lbl_file = os.path.join(path, lbl_file)

    num_labels = -1
    num_images = -1

    with open(lbl_file, 'rb') as f:
        magic, num_labels = struct.unpack(">II", f.read(8))
        if magic != LBL_MAGIC_NUM:
            print('label magic number wrong')
        lbl = np.fromfile(f, dtype=np.int8)
        if len(lbl) != num_labels:
            print('len(lbl) != num_labels ({}!={})'.format(len(lbl), num_labels))

    with open(img_file, 'rb') as f:
        magic, num_images, rows, cols = struct.unpack(">IIII", f.read(16))
        if magic != IMG_MAGIC_NUM:
            print('image magic number wrong')
        if rows != IMG_ROWS or cols != IMG_COLS:
            print('image size not supported ({}x{} != {}x{})'.format(rows, cols, IMG_ROWS, IMG_COLS))
        if num_labels != num_images:
            print('must have same images than labels ({}!={})'.format(num_images, num_labels))
        img = np.fromfile(f, dtype=np.uint8).reshape(num_labels, rows*cols)
        if len(img) != num_images:
            print('len(img) != num_image ({}!={})'.format(len(img), num_images))
    for i in range(len(lbl)):
        yield (lbl[i], img[i])

def train_network(hidden_nodes=200, learning_rate=0.1, epochs=10):
    input_nodes = 784
    output_nodes = 10
    path = os.path.join('nnmodel', DEFAULT_MODEL_FILE)
    n = NeuralNetwork(input_nodes,hidden_nodes,output_nodes, learning_rate)
    if os.path.isfile(path):
        n.load_state_dict(torch.load(path))
    else:
        training_dataset = read_dataset(
            img_file='train-images.idx3-ubyte',
            lbl_file='train-labels.idx1-ubyte',
            path='data')
        for e in range(epochs):
            for (label, image) in training_dataset:
                inputs = (np.asfarray(image) / 255.0 * 0.99) + 0.01
                targets = np.zeros(output_nodes) + 0.01
                targets[label] = 0.99
                n.train(inputs, targets)

        testing_dataset = read_dataset(
                img_file='t10k-images.idx3-ubyte', 
                lbl_file='t10k-labels.idx1-ubyte', 
                path='data')
        scorecard = []
        for (correct_label, image) in testing_dataset:
            inputs = (np.asfarray(image) / 255.0 * 0.99) + 0.01
            outputs = n.forward(inputs)
            m, label = outputs.max(1)
            scorecard.append(1) if (label.data[0] == correct_label) else scorecard.append(0)
        scorecard_array = np.asarray(scorecard)
        performance = scorecard_array.sum() / scorecard_array.size
        if performance > 0.9:
            torch.save(n.state_dict(), path)
    return n

def guess_number(image):
    n = train_network()
    outputs = n.forward(image)
    m, label = outputs.max(1)
    return label.data[0]

