import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageDraw
from src import nn


IMAGE_WIDTH  = 200
IMAGE_HEIGHT = 200


class MNIST(tk.Tk):
    def left_drag(self, event):
        color = 'black'
        radius = self.slider.get()

        if self.last_x is not None and self.last_y is not None:
            self.canvas.create_line(
                    self.last_x, self.last_y, 
                    event.x, event.y, 
                    width=radius, fill=color, 
                    capstyle=tk.ROUND, 
                    smooth=True, 
                    splinesteps=36)
            self.dimg.line([self.last_x, self.last_y, event.x, event.y] , fill=color, width=radius)
        self.last_x, self.last_y = (event.x, event.y)

    def guess_number(self, inverted=True):
        tmp_img = self.mimg.convert('L')
        tmp_img = tmp_img.resize((28,28), Image.ANTIALIAS)
        pixels = []
        for x in range(28):
            for y in range(28):
                if inverted:
                    pixels.append(255 - tmp_img.getpixel((y, x)))
                else:
                    pixels.append(tmp_img.getpixel((y, x)))
        self.guessed = nn.guess_number(pixels)
        messagebox.showinfo('Prediction', 'The NN thinks that you drow a {}'.format(self.guessed))

    def clear_canvas(self):
        self.canvas.delete('all')
        self.mimg = Image.new('RGB', (IMAGE_HEIGHT, IMAGE_WIDTH), 'white')
        self.dimg = ImageDraw.Draw(self.mimg)

    def reset(self, event=None):
        self.last_x = None
        self.last_y = None

    def setup(self):
        self.reset()
        self.mimg = Image.new('RGB', (IMAGE_HEIGHT, IMAGE_WIDTH), 'white')
        self.dimg = ImageDraw.Draw(self.mimg)

    def load_file(self):
        from tkinter.filedialog import askopenfilename
        fname = askopenfilename(filetypes=[('PNG file', '*.png')])
        img = Image.open(fname)
        self.mimg = img
        self.guess_number(inverted=False)
        self.reset()
        self.clear_canvas()

    def __init__(self):
        super().__init__()
        self.setup()
        self.slider_label = tk.Label(self, text='Brush width')
        self.slider_label.pack(side=tk.TOP)
        self.slider = tk.Scale(self, from_=10, to=30, orient=tk.HORIZONTAL)
        self.slider.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        self.canvas = tk.Canvas(self, width=IMAGE_WIDTH, height=IMAGE_HEIGHT)
        self.canvas.bind('<B1-Motion>', self.left_drag)
        self.canvas.bind('<ButtonRelease-1>', self.reset)
        self.canvas.configure(background='white')
        self.canvas.pack()
        self.load_button = tk.Button(self, text='Load Image', command=self.load_file, padx=5, pady=5)
        self.load_button.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.clear_canvas = tk.Button(self, text='Clear', command=self.clear_canvas, padx=5, pady=5)
        self.clear_canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.guess_button = tk.Button(self, text='Guess Number', command=self.guess_number, padx=5, pady=5)
        self.guess_button.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

if __name__ == '__main__':
    root = MNIST()
    root.mainloop()

